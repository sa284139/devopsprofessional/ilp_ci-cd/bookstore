### BookStore

```
ILP CI-CD project - BookStore
```
---

### Build Tool

```
maven
```
---

### Steps to Build locally

1. Clone BookStore repo
```
git clone git@gitlab.com:sa284139/devopsprofessional/ilp_ci-cd/bookstore.git
```

2. Build
```
maven clean install
```
---
